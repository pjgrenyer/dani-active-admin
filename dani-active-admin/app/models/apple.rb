class Apple < ActiveRecord::Base
  belongs_to :colour

  def to_s
    colour
  end
end
