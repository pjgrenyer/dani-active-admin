# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Apple.delete_all
Colour.delete_all

Colour.create!(colour: 'Black')
Colour.create!(colour: 'Red')

Apple.create!(name: 'Cox')
Apple.create!(name: 'Golden Deliceous')