class CreateAppleColours < ActiveRecord::Migration
  def change
    create_table :apple_colours do |t|
      t.references :apple, index: true
      t.references :colour, index: true

      t.timestamps
    end
  end
end
