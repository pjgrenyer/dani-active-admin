class AddColoursToApple < ActiveRecord::Migration
  def change
    add_reference :apples, :colour, index: true
  end
end
